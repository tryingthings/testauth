//
//  FirebaseJSON.swift
//  TestAuth
//
//  Created by trying-things on 25/06/2018.
//  Copyright © 2018 trying-things. All rights reserved.
//

import UIKit
import SwiftyJSON

class FirebaseJSON: NSObject {
    
    let firebase = Firebase.shared
    
    static func getMinutesAvailable(_ userInfo: JSON) -> String {
        let minutes = userInfo["minutes available"].stringValue
        return minutes
    }
    
    static func getStartTime(_ userInfo: JSON) -> String {
        let start = userInfo["start"].stringValue
        return start
    }
    
}
