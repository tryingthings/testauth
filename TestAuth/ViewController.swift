//
//  ViewController.swift
//  TestAuth
//
//  Created by trying-things on 23/06/2018.
//  Copyright © 2018 trying-things. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let database = Firebase.shared
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var minutesTextField: UITextField!
    
    var pressedCall = false
    var connectedCall = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.endVoice(_:)), name: .endVoice, object: nil)
    }

    @IBAction func pressCall(_ sender: Any) {
        
        if pressedCall == false {
            callButton.setTitle("Hang up", for: .normal)
            pressedCall = true
            database.pressCall(minutesTextField.text!)
        } else if pressedCall == true {
            callButton.setTitle("Start call", for: .normal)
            pressedCall = false
            database.pressHangUp()
        }
    }
    
    @objc func endVoice(_ notification: Notification) {
        // Only hang up using the timer if they're not in a call
        if connectedCall == false {
            if pressedCall == true {
                callButton.setTitle("Start call", for: .normal)
                pressedCall = false
                database.pressHangUp()
            }
        }
    }
}

