//
//  Firebase.swift
//  TestAuth
//
//  Created by trying-things on 23/06/2018.
//  Copyright © 2018 trying-things. All rights reserved.
//

import UIKit
import Firebase
import SwiftyJSON

class Firebase: NSObject {
    
    // Use Firebase.shared to use a single instance anywhere
    public static let shared = Firebase()
    
    // Save the user's ID and its type, eg: ("anonymous, "EGnhg3jvumTbXBiraDv0Tq142KK2")
    var userID = ("type","ID")
    var usersWaitingForVoice = [String: [String: Any]]()
    
    var endTimer = Timer()

    
    // Wait until a read/write request before getting the database reference
    var ref: DatabaseReference!
    var waitingForVoice: DatabaseReference!
    
    func configure() {
        waitingForVoice = Database.database().reference().child("waiting for voice")
        observeWaitingForVoice()
    }
    

    func signInAnonymously() {
        // Automatically give an ID of random letters/numbers to the user
        Auth.auth().signInAnonymously() { (authResult, error) in
            if error == nil {
                if let user = authResult?.user {
                    let isAnonymous = user.isAnonymous  // true
                    let uid = user.uid // eg. EGnhg3jvumTbXBiraDv0Tq142KK2
                    print("User ID \(uid) is anonymous: \(isAnonymous)")
                    // Save ID to variable on line 18
                    self.userID = ("anonymous",uid)
                }
            } else {
                print("Firebase Auth error: \(String(describing: error))")
            }
        }
    }
    
    // Press the call button
    func pressCall(_ minutes: String) {
        print("press call")
        // Add this user's ID to a list of people who want to talk
        waitingForVoice.child("\(userID.1)").setValue(["start": "\(Date())","minutes available":minutes])
    }
    
    // Press the hang up button
    func pressHangUp() {
        print("press hang up")
        // Remove this user's ID from the list of people who want to talk
        waitingForVoice.child("\(userID.1)").removeValue()
        cancelTimer()
    }
    
    // If changes occur, get a snapshot of the database
    func observeWaitingForVoice() {
        waitingForVoice.observe(.value, with: { (snapshot) in
            guard snapshot.exists() else {
                print("Users waiting for voice: [none]")
                self.usersWaitingForVoice.removeAll()
                return
            }
            // Convert snapshot to dictionary
            let snapshotDictionary = snapshot.value as? [String: AnyObject] ?? [:]
            // Empty existing list of users waiting for voice
            self.usersWaitingForVoice.removeAll()
            // Add each user ID to the waiting list
            for (userID, userInfo) in snapshotDictionary {
                let timeStarted = FirebaseJSON.getStartTime(JSON(userInfo))
                let minutesAvailable = FirebaseJSON.getMinutesAvailable(JSON(userInfo))
                let minutesWaiting = self.getMinutesWaiting(start: timeStarted, minutesAvailable: minutesAvailable)
                let endTime = self.getEndTime(start: timeStarted, minutesAvailable: minutesAvailable)
                self.usersWaitingForVoice[userID] = ["time started": timeStarted, "minutes available": minutesAvailable, "minutes waiting": minutesWaiting.0, "percentage done": minutesWaiting.1, "end time": endTime]
                self.startTimer(endTime)
                }
            print("Users waiting for voice: \(self.usersWaitingForVoice)")
        })
    }
    
    func startTimer(_ time: Date) {
        endTimer = Timer.init(fireAt: time, interval: 0, target: self, selector: #selector(notifyEndTime), userInfo: nil, repeats: false)
        RunLoop.main.add(endTimer, forMode: .commonModes)
        print("set timer for \(time)")
    }
    
    @objc func notifyEndTime() {
        NotificationCenter.default.post(Notification(name: .endVoice))
        print("sent notification to hang up")
    }
    
    @objc func cancelTimer() {
        endTimer.invalidate()
    }
    
    func getEndTime(start: String, minutesAvailable: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss xxxxx"
        guard let startTime = dateFormatter.date(from: start) else {
            return Date()
        }
        
        guard var minutesAvailableInt = Double(minutesAvailable) else {
            return Date()
        }
        minutesAvailableInt = minutesAvailableInt*60
        let endTime = startTime.addingTimeInterval(minutesAvailableInt)
        return endTime
    }

    func getMinutesWaiting(start: String, minutesAvailable: String) -> (Double, Double) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss xxxxx"
        guard let startTime = dateFormatter.date(from: start) else {
            return (0.0, 0.0)
        }
        let currentTime = Date()
        let difference = currentTime.timeIntervalSince(startTime)
        let minutes = difference/60
        
        guard let minutesAvailableInt = Double(minutesAvailable) else {
            return (0.0, 0.0)
        }
        
        let percentage = (minutes/minutesAvailableInt)*100
        
        return (minutes, percentage)
    }
}


